// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries
import { FacebookAuthProvider } from '@firebase/auth';

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyDM5sVrT49AobeKy03Cestp3HSDqoz16f0",
    authDomain: "shiba-52466.firebaseapp.com",
    projectId: "shiba-52466",
    storageBucket: "shiba-52466.appspot.com",
    messagingSenderId: "94218593777",
    appId: "1:94218593777:web:a9d73fe8112722a4601050"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export default app;