import React, { useState, useEffect } from 'react';
import DownIcon from 'mdi-react/ChevronDownIcon';
import { Collapse } from 'reactstrap';
import TopbarMenuLink from './TopbarMenuLink';

const Ava = `${process.env.PUBLIC_URL}/img/ava.png`;

const TopbarProfile = () => {
    const [isCollapsed, setIsCollapsed] = useState(false);
    const [profile, setProfile] = useState({});
    const handleToggleCollapse = () => {
        setIsCollapsed(!isCollapsed);
    };

    useEffect(() => {
        const fb = JSON.parse(localStorage.getItem('fb'));
        setProfile(fb);
    }, []);

    return (
        <div className="topbar__profile">
            <button type="button" className="topbar__avatar" onClick={handleToggleCollapse}>
                <img className="topbar__avatar-img" src={profile?.picture?.data?.url ?? Ava} alt="avatar" />
                <p className="topbar__avatar-name">{profile?.name ?? '-'}</p>
                <DownIcon className="topbar__icon" />
            </button>
            {isCollapsed && (
                <button
                    type="button"
                    aria-label="button collapse"
                    className="topbar__back"
                    onClick={handleToggleCollapse}
                />
            )}
            <Collapse isOpen={isCollapsed} className="topbar__menu-wrap">
                <div className="topbar__menu">
                    {/* <TopbarMenuLink title="Page one" icon="list" path="/pages/one" />
                    <TopbarMenuLink title="Page two" icon="inbox" path="/pages/two" /> */}
                    {/* <div className="topbar__menu-divider" /> */}
                    {/* /// log out tur zuur hiiv */}
                    <button className="topbar__link" onClick={() => {
                        localStorage.clear();
                        window.location.reload();
                    }} >
                        <span className={`topbar__link-icon lnr lnr-exit`} />
                        <p className="topbar__link-title">Log Out</p>
                    </button>
                    {/* <TopbarMenuLink title="Log Out" icon="exit" path="/" /> */}
                </div>
            </Collapse>
        </div>
    );
};

export default TopbarProfile;
