import React, { useEffect, useRef } from 'react';
import './login.scss';
import { Button } from 'reactstrap';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props'
import FacebookIcon from 'mdi-react/FacebookIcon';
import firebase from '../../config/firebase';
import { FacebookAuthProvider, signInWithPopup, getAuth } from '@firebase/auth';

const FB_APP_ID = '626468365133811';

const fbProfile = {
    "name": "Bayaraa Sanjaa",
    "email": "bsajnaa123109@gmail.com",
    "picture": {
        "data": {
            "height": 50,
            "is_silhouette": true,
            "url": "https://scontent.fuln2-2.fna.fbcdn.net/v/t1.30497-1/cp0/c15.0.50.50a/p50x50/84628273_176159830277856_972693363922829312_n.jpg?_nc_cat=1&ccb=1-5&_nc_sid=12b3be&_nc_ohc=noPVX-74Y9YAX-Hxhsq&_nc_ht=scontent.fuln2-2.fna&edm=AP4hL3IEAAAA&oh=00_AT_HBQdJNvRR3qrywEHx_GmYzoukFcYP4DliBLuFYPZFvQ&oe=6211C338",
            "width": 50
        }
    },
    "id": "32132132121321414",
    "accessToken": "dhajkhdajksfhkdjfhjdfdSFDSJGJKSFGsf",
    "userID": "32132132121321414",
    "expiresIn": 4655,
    "signedRequest": "Tfl95CcCRF1wZZWB_UrCajewqewqdasJJHXxm1xFfSkBA3r4KDZE.eyJ1c2VyX2lkIjoiNDU1OTczMDU2NzQ1OTU1OCIsImNvZGUiOiJBUUN3Xzd4LW8xdTlQUG50N2ZGemZtSDBpcFVZOUZ1dkxhSGIzZVZROFdybUJBWDkwOUdaakI0VDhzbThfTmlVOGpxV05XSi00Y3FSNk9NX2diTWhmc1drYW1mRU5kX0N4MnRublJKejZxNEZVX1ZCeEZmZlluNk5YdXUxT1hqY2d6Q21kRjZ1UHRCa0VLUE5FSndlYVBGRjFLdVJVY2ZlNTk4c3NlVUQ5V0VIQjhWUFFnb0txWUxlR3NSNWc4SlRBM1JIcU94T2RkNEZjOVBBbHpEQnRCc0wyazVUekg4R0EzYkJEcm5JSlY1Y0Q1RG9wTElwZzJ2SHd6ajFjV2pDSm1QeWlURGsyUjBIRHk4U1NYZFA3TXdDajNyejdQU1FFMU5MSWxSZktoV0l4bHkwekRYWF85SFRHMGtmR3N6bzdfNU1RVU1sdklqMFdYczJoc3VTU2pfdGVFTmpHZUxxa28tMm9nZnNFMG4tSmciLCJhbGdvcml0aG0iOiJITUFDLVNIQTI1NiIsImlzc3VlZF9hdCI6MTY0Mjg4NDE0NX0",
    "graphDomain": "facebook",
    "data_access_expiration_time": 1650660145
};

const NewLogin = () => {

    const videoRef = useRef(null);

    useEffect(() => {
        videoRef?.current?.play?.();
    }, []);

    console.log(firebase)

    // const facebookProvider = firebase.auth.FacebookAuthProvider();

    return (
        <div className='login-wrapper'>
            <video className='bg-video' ref={videoRef} width="100%" height="100%" controls={false} autoPlay muted loop>
                <source src='/videos/login.mp4' type="video/mp4" />
            </video>
            <div className='login-card'>
                <img className='login-logo'
                    src='/img/logo.jpg'
                />
                <div className='login-button-container'>

                    <Button
                        color="info"
                        outline
                        size="sm"
                        className='fb-login-button'
                        onClick={async () => {
                            setTimeout(() => {
                                localStorage.setItem('auth', true);
                                localStorage.setItem('fb', JSON.stringify(fbProfile));
                                window.location.reload();
                            }, 200);
                        }}
                    >
                        Login with Facebook
                    </Button>

                    {/* <FacebookLogin
                        appId={FB_APP_ID}
                        fields="name,email,picture"
                        callback={(response) => {
                            if (response?.userID) {
                                console.log(response)
                                localStorage.setItem('auth', true);
                                localStorage.setItem('fb', JSON.stringify(response));
                                // localStorage.setItem('token', response.accessToken)
                                // window.location.reload();
                            }
                        }}
                        icon={
                            <FacebookIcon />
                        }
                        textButton=''
                        onFailure={() => {
                            console.log('on failure')
                        }}
                        render={renderProps => (
                            <Button
                                color="info"
                                outline
                                size="sm"
                                className='fb-login-button'
                                onClick={renderProps.onClick}
                            >
                                Login with Facebook
                            </Button>
                        )}
                    /> */}

                    {/* <Button
                        color="info"
                        outline
                        size="sm"
                        className='fb-login-button'
                        onClick={() => {

                        }}
                    >
                        Login with Facebook
                    </Button> */}
                    <Button
                        style={{ width: 300, marginRight: 0 }}
                        color="info"
                        outline
                        size="sm"
                    >
                        Бүртгүүлэх
                    </Button>
                </div>
            </div>
        </div >
    )
};

export default NewLogin;