import React from 'react';
import { Link } from 'react-router-dom';
import FacebookIcon from 'mdi-react/FacebookIcon';
import GooglePlusIcon from 'mdi-react/GooglePlusIcon';
import LogInForm from './components/LogInForm';
// import FacebookLogin from 'react-facebook-login';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props'

const FB_APP_ID = '626468365133811';

const LogIn = () => (
    <div className="account">
        <div className="account__wrapper">
            <div className="account__card">
                <div className="account__head">
                    <h3 className="account__title">Welcome to
                        <span className="account__logo"> Easy
                            <span className="account__logo-accent">DEV</span>
                        </span>
                    </h3>
                    <h4 className="account__subhead subhead">Start your business easily</h4>
                </div>
                <LogInForm onSubmit={() => { }} />
                <div className="account__or">
                    <p>Хялбар нэвтрэх</p>
                </div>
                <div className="account__social">
                    <FacebookLogin
                        appId={FB_APP_ID}
                        fields="name,email,picture"
                        callback={(response) => {
                            if (response?.userID) {
                                localStorage.setItem('auth', true);
                                localStorage.setItem('fb', JSON.stringify(response));
                                // localStorage.setItem('token', response.accessToken)
                                window.location.reload();
                            }
                        }}
                        icon={
                            <FacebookIcon />
                        }
                        textButton=''
                        onFailure={() => {
                            console.log('on failure')
                        }}
                        render={renderProps => (
                            <button
                                type='button' // eslint-disable-line
                                className="account__social-btn account__social-btn--facebook"
                                onClick={renderProps.onClick}
                            >
                                <FacebookIcon />
                            </button>
                        )}
                    />
                    {/* <button
                        onClick={() => {
                            window.FB.getLoginStatus();
                        }}
                    >
                        get fb status
                    </button>

                    <button
                        onClick={() => {
                            const token = localStorage.getItem('token');
                            console.log('token>>>', token);
                            window.FB.logout(token);
                        }}
                    >
                        log out of fb
                    </button> */}
                    <Link
                        className="account__social-btn account__social-btn--google"
                        to="/pages/one"
                    >
                        <GooglePlusIcon />
                    </Link>
                </div>
            </div>
        </div>
    </div>
);

export default LogIn;

// if you want to add select, date-picker and time-picker in your app you need to uncomment the first
// four lines in /scss/component/form.scss to add styles
