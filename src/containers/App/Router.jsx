import React, { useState, useEffect } from 'react';
import { Redirect, Route, Switch, useHistory, useLocation } from 'react-router-dom';
import Layout from '../Layout/index';
import MainWrapper from './MainWrapper';
import LogIn from '../LogIn/index';
import ExamplePageOne from '../Example/index';
import ExamplePageTwo from '../ExampleTwo/index';
import Lottery from '../Lottery'
import FinanceDashboard from '../FinanceDashboard';
import NewLogin from '../LogIn/NewLogin';

const Pages = () => (
    <Switch>
        {/* <Route path="/pages/one" component={ExamplePageOne} /> */}
        {/* <Route path="/pages/two" component={ExamplePageTwo} /> */}
        {/* <Redirect from='/' to={'/market'} /> */}
        <Route path="/lottery" component={Lottery} />
        <Route path="/market" component={FinanceDashboard} />
    </Switch>
);

const wrappedRoutes = () => (
    <div>
        <Layout />
        <div className="container__wrap">
            <Route path="/" component={Pages} />
        </div>
    </div>
);

const TestComponent = () => {
    return (
        <div>
            you are successfully authenticated
        </div>
    )
}

const Router = () => {
    const [isAuthenticated, setIsAuthenticated] = useState(false);
    const location = useLocation();
    const { pathname } = location;

    useEffect(() => {
        const auth = localStorage.getItem('auth');
        if (auth === 'true') {
            setIsAuthenticated(true);
        }
    }, [])


    if (!isAuthenticated) {
        if (pathname !== '/') {
            window.location.href = '/';
        }
        return (
            <MainWrapper>
                <main>
                    <Switch>
                        {/* <Redirect from='/*' to='/' /> */}
                        {/* <Route exact path="/" component={LogIn} /> */}
                        <Route exact path="/" component={NewLogin} />
                    </Switch>
                </main>
            </MainWrapper>
        )
    }

    return (
        <MainWrapper>
            <main>
                <Switch>
                    <Route path="/" component={wrappedRoutes} />
                    {/* <Route exact path="/log_in" component={LogIn} />
                    <Route path="/" component={wrappedRoutes} /> */}
                </Switch>
            </main>
        </MainWrapper>
    )
};

export default Router;
