import React, { useEffect, useState } from 'react';
import { Col, Container, Row, Card, CardBody, Button } from 'reactstrap';
import './lottery.scss';


const names = [
    'Alfred Webster',
    'Jean Grant',
    'Chandler Pope',
    'Mohammad Carson',
    'Andres Ibarra',
    'Carsen Summers',
    'Madden Bennett',
    'Brycen Ibarra',
    'Asia Sampson',
    'Andrea Kidd',
    'Britney Stone',
    'Braylon Gomez',
    'Gunner Moran',
    'Lyric Fowler',
    'Maxwell Gallegos',
    'Cassandra Lin',
    'Yair Pollard',
    'Johnathon Lopez',
    'Lexie Bruce',
    'Cole Johnston',
];

const emails = [
    'trygstad@icloud.com',
    'jonadab@icloud.com',
    'zwood@hotmail.com',
    'petersen@msn.com',
    'chrisj@live.com',
    'hllam@yahoo.ca',
    'mwilson@mac.com',
    'tarreau@msn.com',
    'jbarta@aol.com',
    'seasweb@att.net',
    'ribet@live.com',
    'jkegl@aol.com',
    'timtroyr@aol.com',
    'dinther@sbcglobal.net',
    'chrwin@att.net',
    'kingma@gmail.com',
    'krueger@verizon.net',
    'elmer@aol.com',
    'ournews@gmail.com',
    'scottlee@yahoo.ca',
];

const data = {
    firstName: 'Jonondarnad',
    lastName: 'Battuya',
    count: 5,
    avatar: 'https://previews.123rf.com/images/inegvin/inegvin1701/inegvin170100077/69882112-user-sign-icon-person-symbol-human-avatar-.jpg',
};

const types = [
    '5,000',
    '10,000',
    '20,000',
    '50,000',
];

const avatars = [
    'https://images.unsplash.com/photo-1527980965255-d3b416303d12?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Nnx8YXZhdGFyfGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60',
    'https://images.unsplash.com/photo-1628890923662-2cb23c2e0cfe?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTB8fGF2YXRhcnxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=500&q=60',
    'https://images.unsplash.com/photo-1544725176-7c40e5a71c5e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8OHx8YXZhdGFyfGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60',
    'https://images.unsplash.com/photo-1628157588553-5eeea00af15c?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTZ8fGF2YXRhcnxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=500&q=60',
    'https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8YXZhdGFyfGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60'
];

const ShibaComponent = () => {

    const [selectedType, setSelectedType] = useState(types[0]);
    const [query, setQuery] = useState('');
    const [records, setRecords] = useState([]);

    useEffect(() => {
        const fakeRecords = [];

        for (let i = 0; i < 15; i++) {
            const name = names[Math.floor(Math.random() * names.length)];
            const email = emails[Math.floor(Math.random() * emails.length)];
            const avatar = avatars[Math.floor(Math.random() * avatars.length)];

            const [firstName, lastName] = name.split(' ');
            fakeRecords.push({
                ...data,
                firstName,
                lastName,
                count: Math.floor(Math.random() * 21),
                email,
                avatar
            });
        };
        setRecords(fakeRecords);
    }, [selectedType]);



    const getFilteredRecords = () => {

        return records.filter(record => {
            const queryLC = query.toLowerCase();
            const firstNameLC = record.firstName.toLowerCase();
            const lastNameLC = record.lastName.toLowerCase();
            return firstNameLC.includes(queryLC) || lastNameLC.includes(queryLC);
        })

    }


    return (
        <Container className="dashboard">
            <Row>
                <Col md={12}>
                    <h3 className="page-title">Сугалаа</h3>
                </Col>
            </Row>
            <Row>
                <Col md={12}>
                    <Card>
                        <CardBody>
                            <div>
                                {
                                    types.map(type => {
                                        return (
                                            <Button
                                                key={type}
                                                color="success"
                                                size="sm"
                                                onClick={() => {
                                                    setSelectedType(type);
                                                }}
                                                active={selectedType === type}
                                            >
                                                {type}
                                            </Button>
                                        )
                                    })
                                }
                            </div>
                            <div className='search-container'>
                                <span>
                                    Хайх:
                                </span>
                                <input
                                    className='form-control'
                                    placeholder='Хайх утга...'
                                    value={query}
                                    onChange={e => {
                                        setQuery(e.target.value);
                                    }}
                                />
                            </div>
                            <table className='lottery-table'>
                                <thead>
                                    <tr>
                                        <th>№</th>
                                        {/* <th>Овог</th> */}
                                        <th>Нэр</th>
                                        <th>Сугалаа</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        getFilteredRecords().map((record, index) => {
                                            return (
                                                <tr key={index}>
                                                    <td>{index + 1}</td>
                                                    <td>
                                                        <div className='person-detail'>
                                                            <img
                                                                src={record.avatar}
                                                            />
                                                            <div>
                                                                <div>
                                                                    {`${record.firstName} ${record.lastName}`}
                                                                </div>
                                                                <div>
                                                                    {record.email}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    {/* <td>{record.firstName}</td> */}
                                                    <td>{record.count}</td>
                                                </tr>
                                            )
                                        })
                                    }
                                </tbody>
                            </table>

                            {/* <div className="card__title">
                                <h5 className="bold-text">10,000₮</h5>
                                <h5 className="subhead">Example subhead</h5>
                            </div>
                            <p>Your content here</p> */}
                        </CardBody>
                    </Card>
                </Col>
            </Row>
        </Container>
    )
};

export default ShibaComponent;
