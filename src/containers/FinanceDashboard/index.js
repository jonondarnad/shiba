import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import BTC from './BTC';
import ETH from './ETH';
import BCH from './BCH';
import XRP from './XRP';
import TradeHistory from './TradeHistory';
import BtcEth from './BtcEth';
import CryptotrendsToday from './CryptotrendsToday';
// import PlaceOrder from './PlaceOrder';
import TopTen from './TopTen';

const FinanceDashboard = () => {
    return (
        <Container className="dashboard">
            <Row>
                <Col md={12}>
                    <h3 className="page-title">FINANCE DASHBAORD PAGE TITLE</h3>
                </Col>
            </Row>
            <Row>
                <BTC dir={'ltr'} />
                <ETH dir={'ltr'} />
                <BCH dir={'ltr'} />
                <XRP dir={'ltr'} />
            </Row>
            <Row>
                <TradeHistory />
                <BtcEth
                    dir={'ltr'}
                    theme={'theme-light'}
                />
                <CryptotrendsToday dir={'ltr'} />
                {/* <PlaceOrder /> */}
                {/* <TopTen cryptoTable={cryptoTable} onDeleteCryptoTableData={onDeleteCryptoTableData(dispatch, cryptoTable)} /> */}
            </Row>
        </Container>

    )
};

export default FinanceDashboard;